from django.apps import AppConfig


class AppHalamanutamaConfig(AppConfig):
    name = 'app_halamanutama'
