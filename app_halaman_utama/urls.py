from django.conf.urls import url
from .views import index
from django.views.generic.base import RedirectView

urlpatterns = [
    url(r'^$', index, name='index'),
]
