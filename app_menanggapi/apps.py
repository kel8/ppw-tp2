from django.apps import AppConfig


class AppUpdateStatusConfig(AppConfig):
    name = 'app_update_status'
