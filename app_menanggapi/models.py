from django.db import models
from django.forms import ModelForm
from forum_lowongan.models import Forum

class Comment(models.Model):
	content = models.TextField()
	created_on = models.DateTimeField(auto_now_add=True)
	forum = models.ForeignKey(Forum)

class CommentForm(ModelForm):
    class Meta:
        model = Comment
        exclude = ['created_on','forum']