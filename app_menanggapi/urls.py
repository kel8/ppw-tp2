from django.conf.urls import url
from .views import index, tambah_tanggapan

urlpatterns = [
	url(r'^$', index, name='index'),
	url(r'^tambah_tanggapan', tambah_tanggapan, name='tambah_tanggapan'),
    ]
