from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Comment,CommentForm
from forum_lowongan.models import Forum

response = {}

def index(request):
	response['CommentForm'] = CommentForm
	html = 'app_menanggapi/app_menanggapi.html'
	forum = Forum.objects.all()
	response['forum'] = forum
	return render(request, html, response)
    
def tambah_tanggapan(request):
	form = CommentForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['comment'] = request.POST.get('comment')
		response['id'] = request.POST.get('data.id')
		obj = Forum.objects.get(pk=response['id'])
		comment = Comment(response['comment'])
		comment.save()
		return HttpResponseRedirect('/menanggapi/')