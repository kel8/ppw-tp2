from django.test import TestCase, Client
from django.urls import resolve
from .views import index
from .models import Forum

# Create your tests here.
class ForumTest(TestCase):
    def test_forum_url_exists(self):
        response = Client().get('/company/forum/')
        self.assertEqual(response.status_code, 200)

    def test_status_using_index_func(self):
        found = resolve('/company/forum/')
        self.assertEqual(found.func, index)

    def test_model_can_create(self):
        Forum.objects.create(content="I'm currently happy")

        counter_all_available_activity = Forum.objects.all().count()
        self.assertEqual(counter_all_available_activity, 1)

    def test_add_forum_can_POST(self):
        response = self.client.post('/company/forum/add/', data={'title':'Happy', 'content': "I'm currently happy"})
        counter_all_available_activity = Forum.objects.all().count()
        self.assertEqual(counter_all_available_activity, 1)

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/company/forum/')
        new_response = self.client.get('/company/forum/')
        html_response = new_response.content.decode('utf8')
        self.assertIn("currently happy", html_response)

    def test_add_forum_reject_invalid_POST(self):
        response = self.client.post('/company/forum/add/', data={'content': ''})
        html_response = response.content.decode('utf8')
        self.assertNotEqual(response.status_code, 400)

    def test_add_forum_GET_redirects(self):
        response = self.client.get('/company/forum/add/')
        self.assertEqual(response.status_code, 302)

    def test_delete_forum(self):
        forum = Forum.objects.create(content="I'm currently happy")
        response = self.client.get('/company/forum/delete/'+str(forum.id)+'/')
        counter_all_available_activity = Forum.objects.all().count()
        self.assertEqual(counter_all_available_activity, 0)
        self.assertEqual(response.status_code, 302)
    
        new_response = self.client.get('/company/forum/')
        html_response = new_response.content.decode('utf8')
        self.assertNotIn("currently happy", html_response)