from django.shortcuts import render, redirect
from .models import Forum
from .forms import ForumForm
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger

# Create your views here.
response = {}
def index(request):
    forum_list = Forum.objects.all()
    paginator = Paginator(forum_list, 3)

    page = request.GET.get('page')
    try:
        forum = paginator.page(page)
    except:
        forum = paginator.page(1)

    response['forum'] = forum
    return render(request, 'forum.html', response)
def add(request):
    if request.method == 'POST':
        form = ForumForm(request.POST)
        response['form'] = form
        if form.is_valid():
            Forum.objects.create(content=form.cleaned_data['content'])
            forum = Forum.objects.all()
            response['forum'] = forum
            return redirect('/company/forum/')
        else:
            forum = Forum.objects.all()
            response['forum'] = forum
            response['form'] = form
            #return render(request, 'test.html', response)
    return redirect('/company/forum/')

def delete_forum(request, id):
    to_delete = Forum.objects.get(id=id)
    to_delete.delete()
    return redirect('/company/forum/')
