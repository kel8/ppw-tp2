"""ppw_tp2 URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.contrib import admin
from django.views.generic.base import RedirectView
import app_halaman_utama.urls as app_halaman_utama
import app_profile.urls as profile
import forum_lowongan.urls as forum
import app_menanggapi.urls as menanggapi

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^company/profile/', include(profile, namespace='profile')),
    url(r'^company/forum/', include(forum, namespace='forum_lowongan')),
    url(r'^company/menanggapi/', include(menanggapi, namespace='menanggapi')),
    url(r'^halaman_utama/', include(app_halaman_utama,namespace='app_halaman_utama')),
    url(r'^$', RedirectView.as_view(url='halaman_utama/', permanent=True), name='index')

]